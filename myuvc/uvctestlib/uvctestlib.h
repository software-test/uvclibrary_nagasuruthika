#ifndef UVCTESTLIB_H
#define UVCTESTLIB_H
#pragma once
#include <QThread>
#include <QtCore>


#include "opencv2/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "libuvc/libuvc.h"
#include "jpeglib.h"
#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <QImage>

#include "uvctestlib_global.h"
using namespace cv;

class UVCTESTLIB_EXPORT Uvctestlib
{
    Q_OBJECT
public:
    explicit Uvctestlib(QObject *parent = 0);
    ~Uvctestlib();
    void run();
    bool running;
    static void cb(uvc_frame_t *frame, void *ptr);

signals:
    void captureImage(QImage);
};

#endif // UVCTESTLIB_H





#pragma once

#include <QMainWindow>
#include <QDebug>
#include <QMetaType>
#include <QStandardItemModel>
#include <QProcess>
#include "../uvctestlib/uvctestlib_global.h"
#include "../uvctestlib/uvctestlib.h"


Uvctestlib Uvctestlib;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QElapsedTimer timer;
    Uvctestlib *currentFeed;

public slots:
    void onImageReceived(QImage);

private slots:
    void StartButtonClicked();

};
